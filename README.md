# Marathon

## What is this?

Enjoy the thrill of running a marathon in this little game made with [decker](https://github.com/JohnEarnest/Decker).

## Run the game

There are two ways to run the game. Both require the `.deck` file, that you may download from this repo.

1. Run the game in the browser.
    1. Go to the ["web-decker" page](http://beyondloom.com/decker/tour.html)
    2. Click on `File`-> `Open...` and pick the `.deck` file.
2. Run the game locally
    1. Install Decker (see instructions in Decker's [README.md](https://github.com/JohnEarnest/Decker))
    2. Same as step 2 to run the deck in the browser (see above).
