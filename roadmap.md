# art assets
- background image for endline
- background image for race, possibly several for variety
- several sprites for runner (animated)
- art for last screen (runner out of breath & sweaty)

# code
- speed in Player module
- remove end screen zone widget
- y-position of runner defined by mouse position
